package controller;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import model.MasterGame;
import static model.MasterGame.EMO_COLOR;
import model.MasterGame.Color;
import model.MasterScore;
import view.MasterView;
import view.buttons.*;

/**
 * MVC stylish controller.
 */
public class MasterController {
	private MasterView view;
	private MasterGame game;
	private MasterScore score;

	/**
	 * Constructs the class.
	 * Adds the view object a <code>MenuButtonListener</code>.
	 */
	public MasterController(MasterView view) {
		this.view = view;
		view.addMenuButtonListeners(new MenuButtonListener());
	}

	/**
	 * Menu view's buttons' listener.
	 */
	class MenuButtonListener implements ActionListener {
		/**
		 * <code>MenuButtonListener</code>'s <code>actionPerformed</code>.
		 * If "Uusi peli" button, then creates a <code>MasterGame</code> object and uses the view object's <code>startGame()</code>.
		 * Then adds all the game view button listeners to the view object.
		 * If "Pelitulokset" button, then creates a <code>MasterScore</code> object and uses the view object's <code>startScore()</code>.
		 * Then adds all the score view button listeners to the view object.
		 * If "Lopeta" button, then uses <code>System.exit(0)</code> to close the game.
		 */
		public void actionPerformed(ActionEvent e) {
			JButton button = (JButton) e.getSource();
			if (button.getText().equals("Uusi peli")) {
				game = new MasterGame();
				view.startGame();
				view.addColorButtonListeners(new ColorButtonListener());
				view.addPegListeners(new PegListener());
				view.addArrowListeners(new ArrowListener());
				view.addGameEndButtonListeners(new EndButtonListener());
			} else if (button.getText().equals("Pelitulokset")) {
				score = new MasterScore();
				view.startScore(score.getScore());
				view.addScoreEndButtonListeners(new EndButtonListener());
			} else if (button.getText().equals("Lopeta"))
				System.exit(0);
			}
	}

	/**
	 * Game view's ColorButtons' listener.
	 * Reads the pushed button, then gives it's color as an argument to <code>view.setChosenColor()</code>.
	 */
	class ColorButtonListener implements ActionListener {
		/**
		 * <code>ColorButtonListener</code>'s <code>actionPerformed</code>.
		 * Set's view's <code>chosenColor</code> to the pressed <code>ColorButton</code>'s color.
		 */
		public void actionPerformed(ActionEvent e) {
			ColorButton button = (ColorButton) e.getSource();
			view.setChosenColor(button.getColor());
		}
	}

	/**
	 * Game view's pegs' listener.
	 */
	class PegListener implements ActionListener {
		/**
		 * <code>PegListener</code>'s <code>actionPerformed</code>.
		 * Checks if <code>chosenColor</code> is not <code>EMO_COLOR</code>.
		 * If not, then changes the game's and view's peg's color according to <code>chosenColor</code>.
		 * In the end uses <code>view.setChosenColor()</code> to set the <code>chosenColor</code> to <code>EMO_COLOR</code>.
		 */
		public void actionPerformed(ActionEvent e) {
			Peg peg = (Peg) e.getSource();
			if (view.getChosenColor() != EMO_COLOR) {
				Color color = view.getChosenColor();
				game.setColor(peg.getPosition(), color);
				peg.setColor(color);
				view.setChosenColor(EMO_COLOR);
			}
		}
	}
	
	/**
	 * Games view's arrows' listener.
	 */
	class ArrowListener implements ActionListener {
		/**
		 * <code>ArrowListener</code>'s <code>actionPerformed</code>.
		 * If the guess is ready to be checked, checks it and moves the game on to the next round.
		 * Then if the game is over, tells the game to save the score.
		 * Finally, tells the view to move on to the next round and passes the hint on to it.
		 */
		public void actionPerformed(ActionEvent e) {
			if (game.isGuessReady() == true) {
				int[] hint = game.checkGuess();
				if (!game.isLastRound()) {
					game.nextRound();
					view.nextRound(game.getRound(), hint);
				} else {
					game.saveScore();
					view.endGame(game.getRound(), game.getCode(), hint);
				}
			}
		}
	}
	
	/**
	 * Game view's and score view's buttons' listener.
	 */
	class EndButtonListener implements ActionListener {
		/**
		 * <code>EndButtonListener</code>'s <code>actionPerformed</code>.
		 * Starts menu view and adds a <code>MenuButtonListener</code> to menu view's buttons.
		 */
		public void actionPerformed(ActionEvent e) {
			view.startMenu();
			view.addMenuButtonListeners(new MenuButtonListener());
		}
	}
}
package model;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;

/**
 * Loads and saves knowledge of games won & lost.
 */
public class FileIO {
	
	/**
	 * Loads the scores from score.tmp.
	 *
	 * @return int[] table containing wins and losses.
	 */
	public int[] loadScore() {
		int[] score = new int[2];
		try {
	    	FileInputStream file  = new FileInputStream("score.tmp");
	    	ObjectInputStream inputStream = new ObjectInputStream(file);
			score = (int[]) inputStream.readObject();
			inputStream.close();
		} catch (Exception e) {
			score[0] = 0;
			score[1] = 0;
		}
		return score;
	}

	/**
	 * Saves the scores to score.tmp.
	 * First loads the old scores, then adds a win or loss and lastly saves the fixed scores.
	 *
	 * @param win if the game was won or lost.
	 */
	public void saveScore(boolean win) {
		int[] score = loadScore();
		if (win)
			score[0]++;
		else
			score[1]++;
		try {
			FileOutputStream file = new FileOutputStream("score.tmp");
			ObjectOutputStream outputStream = new ObjectOutputStream(file);
			outputStream.writeObject(score);
			outputStream.flush();
			file.close();
		} catch (IOException e) {
			System.out.println("Virhe kirjoittaessa.");
		}
	}
} 
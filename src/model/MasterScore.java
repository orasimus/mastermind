package model;

import model.FileIO;

/**
 * Score reader.
 */
public class MasterScore {
	private int[] score = new int[3];

	/**
	 * Constructs the class.
	 * Uses FileIO to read the scorefile and then adds up won and lost games to total game count.
	 */
	public MasterScore() {
		int[] wonlost = new int[2];
		FileIO reader = new FileIO();
		wonlost = reader.loadScore();
		this.score[1] = wonlost[0];
		this.score[2] = wonlost[1];
		this.score[0] = this.score[1] + this.score[2];
	}
	
	/**
	 * Returns the score table.
	 *
	 * @return int[] table containing count of total, won and lost games
	 */
	public int[] getScore() {
		return this.score;
	}
}
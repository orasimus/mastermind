package model;

/**
 * Game logic.
 */
public class MasterGame {
	public final static int MAX_GUESSES = 10;
	public final static int MAX_PEGS_PER_ROUND = 4;
	public final static int MAX_COLORS = Color.values().length - 1;
	public final static Color EMO_COLOR = Color.values()[MAX_COLORS];
	
	/**
	 * Enums that represent button & peg colors.
	 * The last one is the "empty" EMO_COLOR and others are actual play buttons.
	 */
	public enum Color {
		RED, BLUE, GREEN, YELLOW, BROWN, GRAY, BLACK;
		
		/**
		 * Returns the name of the enum as a <code>String</code> in lower case.
		 *
		 * @return String name of the enum in lower case
		 */
		public String toString() {
			return super.toString().toLowerCase();
		}
	}

	private Color[] code = new Color[MAX_PEGS_PER_ROUND];
	private int round;
	private Color[][] guess = new Color[MAX_GUESSES][MAX_PEGS_PER_ROUND];

	/**
	 * Constructs the class.
	 * Sets current round to zero, creates the <code>code</code> to guess and sets all the pegs in <code>guess[][]</code> to <code>EMO_COLOR</code>;
	 *
	 */
	public MasterGame() {
		this.round = 0;
		this.createCode();
		for (int i = 0; i < MAX_GUESSES; i++) {
			for (int j = 0; j < MAX_PEGS_PER_ROUND; j++) {
				this.guess[i][j] = EMO_COLOR;
			}
		}
	}

	/**
	 * Creates the code for the player to guess.
	 *
	 */
	private void createCode() {
		for (int i = 0; i < MAX_PEGS_PER_ROUND; i++) {
			this.code[i] = Color.values()[(int) (Math.random()*MAX_COLORS)];
		}
	}
	
	/**
	 * Returns the code
	 *
	 * @return Color[] the code
	 */
	public Color[] getCode() {
		return this.code;
	}
	
	/**
	 * Returns the current round.
	 *
	 * @return int the current round
	 */
	public int getRound() {
		return this.round;
	}

	/**
	 * Returns the whole array of guesses.
	 *
	 * @return Color[][] array of guesses
	 */
	public Color[][] getGuess() {
		return this.guess;
	}

	/**
	 * Sets the specified peg's color.
	 * Uses the current <code>round</code> automatically.
	 *
	 * @param pegpos the peg position (on the current <code>round</code>)
	 * @param color the color to set
	 */
	public void setColor(int pegpos, Color color) {
		if (isPegPositionValid(pegpos))
			this.guess[this.round][pegpos] = color;
	}

	/**
	 * Checks the current guess and returns the hint.
	 *
	 * @return int[] table that consists of the # of blacks and of whites
	 * @throws Exception the guess is not ready
	 */
	public int[] checkGuess() {
		int black = 0;
		int white = 0;
		int[] done_guess = new int[MAX_PEGS_PER_ROUND];
		int[] done_code = new int[MAX_PEGS_PER_ROUND];
		
		// Check for "blacks" - pegs with correct color on matching location
		for (int i = 0; i < MAX_PEGS_PER_ROUND; i++) {
			if (this.guess[this.round][i].equals(this.code[i])) {
				black++;
				done_guess[i] = 1;
				done_code[i] = 1;
			}
		}
		// Check for "whites" - pegs with correct color, not matching locations
		for (int i = 0; i < MAX_PEGS_PER_ROUND; i++) {
			if (done_guess[i] != 1) {
				for (int j = 0; j < MAX_PEGS_PER_ROUND; j++) {
					if (this.guess[this.round][i].equals(this.code[j]) && done_code[j] != 1) {
						white++;
						done_code[j] = 1;
						break;
					}
				}
			}
		}
		int[] hint = {black, white};
		return hint;
	}

	/**
	 * Sets the round to the next.
	 *
	 */
	public void nextRound() {
		if (!this.isLastRound())
			this.round++;
	}

	/**
	 * Saves final outcome to file
	 *
	 * @throws Exception game is not over
	 */
	public void saveScore() {
		boolean won;
		int[] hint = this.checkGuess();
		if (hint[0] == MAX_PEGS_PER_ROUND)
			won = true;
		else
			won = false;
		FileIO writer = new FileIO();
		writer.saveScore(won);
	}

	/**
	 * Checks if the given peg position is valid.
	 * Valid: 0-3.
	 *
	 * @param pegpos proposed peg position
	 * @return boolean is peg position valid
	 */
	private boolean isPegPositionValid(int pegpos) {
		if (pegpos < 0 || pegpos > 3)
			return false;
		return true;
	}

	/**
	 * Checks if the current round's guess is ready to be checked.
	 *
	 * @return boolean is guess ready to be checked	
	 */
	public boolean isGuessReady() {
		for (int i = 0; i < MAX_PEGS_PER_ROUND; i++) {
			if (this.guess[this.round][i] == EMO_COLOR)
				return false;
		}
		return true;
	}

	/**
	 * Checks if the the current round is the last one.
	 * Returns false if current round is not the last one or if the current guess doesn't have 4 blacks.
	 * Else returns true.
	 *
	 * @return boolean have all rounds been played
	 */
	public boolean isLastRound() {
		int[] hint = this.checkGuess();
		if (MAX_GUESSES > (this.round + 1) && hint[0] != MAX_PEGS_PER_ROUND)
			return false;
		return true;
	}
}
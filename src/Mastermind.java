import view.MasterView;
import controller.MasterController;

/**
 * Game starting class.
 */
public class Mastermind {
	/**
	 * Main method to start the game.
	 * Creates a <code>MasterView</code> view, then creates a <code>MasterController</code> and passes it the view.
	 */
	public static void main(String[] args) {
		MasterView view = new MasterView();
		new MasterController(view);
	}
}
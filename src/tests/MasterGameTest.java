package tests;

import model.*;
import model.MasterGame.Color;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Class for testing MasterGame class' methods.
 */
public class MasterGameTest {
	
	/**
	 * Method for testing constructor.
	 * <ul>
	 * <li>creates object MasterGame, then checks that getRound() returns 0 (round has been properly initialized)</li>
	 * <li>tests that getGuess() array is correct size [MAX_GUESSES][MAX_PEGS_PER_ROUND]</li>
	 * <li>tests that getCode() array is [MAX_PEGS_PER_ROUND]</li>
	 * </ul>
	 */
	@Test
	public void testConstructor() {
		MasterGame game = new MasterGame();
		assertEquals(0, game.getRound());
		assertEquals(MasterGame.MAX_GUESSES, game.getGuess().length);
		assertEquals(MasterGame.MAX_PEGS_PER_ROUND, game.getGuess()[0].length);
		assertEquals(MasterGame.MAX_PEGS_PER_ROUND, game.getCode().length);
	}
	
	/**
	 * Method for testing setColor().
	 * <ul>
	 * <li>uses setColor() to set position 0 color as Color.BLUE, then checks if getGuess() arrays position [0][0] is Color.BLUE</li>
	 * <li>tries to use setColor() to set an invalid position (10) as Color.Brown, then checks that getGuess() array doesn't have a Color.BROWN</li>
	 * </ul>
	 */
	@Test
	public void testSetColor() {
		MasterGame game = new MasterGame();
		game.setColor(0, Color.BLUE);
	    assertEquals(Color.BLUE, game.getGuess()[0][0]);
		
	    game.setColor(10, Color.BROWN);
	    for (int i = 0; i < MasterGame.MAX_GUESSES; i++) {
	    	for (int j = 0; j < MasterGame.MAX_PEGS_PER_ROUND; j++) {
	    		assertFalse(Color.BROWN.equals(game.getGuess()[i][j]));
	    	}
	    }
	}
	
	/**
	 * Method for testing nextRound().
	 * <ul>
	 * <li>uses nextRound() to increase round by one, then checks that getRound() returns 1</li>
	 * <li>uses nextRound() 10 times, then checks that getRound() returns MAX_GUESSES - 1</li>
	 * </ul>
	 */
	@Test
	public void testNextRound() {
		MasterGame game = new MasterGame();
		game.nextRound();
		assertEquals(1, game.getRound());
		
		for (int i = 0; i < 10; i++) {
			game.nextRound();
		}
		assertEquals(MasterGame.MAX_GUESSES - 1, game.getRound());
	}

	/**
	 * Method for testing checkGuess().
	 * <ul>
	 * <li>uses getCode() for the correct code, then sets the first two pegs correct</li>
	 * <li>checks that checkGuess() returns two correct colors on correct positions</li>
	 * <li>sets the last two pegs as correct, then checks that checkGuess() returns four correct colors on correct positions</li>
	 * </ul>
	 */
	@Test
	public void testCheckGuess() {
		MasterGame game = new MasterGame();
		Color[] koodi = game.getCode();
		game.setColor(0, koodi[0]);
		game.setColor(1, koodi[1]);
		assertEquals(2, game.checkGuess()[0]);
		assertEquals(0, game.checkGuess()[1]);
		
		game.setColor(2, koodi[2]);
		game.setColor(3, koodi[3]);
		assertEquals(4, game.checkGuess()[0]);
		assertEquals(0, game.checkGuess()[1]);
	}
	
	/**
	 * Method for testing isGuessReady().
	 * <ul>
	 * <li>checks that isGuessReady() returns false</li>
	 * <li>sets the first 3 pegs, then checks that isGuessReady() returns false</li>
	 * <li>sets the last peg as EMO_COLOR, then checks that isGuessReady() returns false</li>
	 * <li>sets the last peg as valid color Color.GREEN, then checks that isGuessReady() returns true</li>
	 * </ul>
	 */
	@Test
	public void testIsGuessReady() {
		MasterGame game = new MasterGame();
		assertFalse(game.isGuessReady());
		
		game.setColor(0, Color.YELLOW);
		game.setColor(1, Color.BROWN);
		game.setColor(2, Color.GRAY);
		assertFalse(game.isGuessReady());
		
		game.setColor(3, MasterGame.EMO_COLOR);
		assertFalse(game.isGuessReady());
		
		game.setColor(3, Color.GREEN);
		assertTrue(game.isGuessReady());
	}
	
	/**
	 * Method for testing isLastRound().
	 * <ul>
	 * <li>checks that isLastRound() returns false</li>
	 * <li>calls nextRound() 10 times, then checks that isLastRound() returns true</li>
	 * </ul>
	 */
	@Test
	public void testIsLastRound() {
		MasterGame game = new MasterGame();
		assertFalse(game.isLastRound());
		
		for (int i = 1; i <= 10; i++) {
			game.nextRound();
		}
		assertTrue(game.isLastRound());
	}

	/**
	 * Method for testing isLastRound(), in the case that the guess is correct.
	 * <ul>
	 * <li>sets all colors according to getCode(), then checks that isLastRound() returns true</li>
	 * </ul>
	 */
	@Test
	public void testIsLastRoundGuessReady() {
		MasterGame game = new MasterGame();
		Color[] koodi = game.getCode();
		game.setColor(0, koodi[0]);
		game.setColor(1, koodi[1]);
		game.setColor(2, koodi[2]);
		game.setColor(3, koodi[3]);
		assertTrue(game.isLastRound());
	}
}

package view.buttons;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Dimension;

import static model.MasterGame.EMO_COLOR;
import model.MasterGame.Color;

/**
 * Game view's peg.
 */
public class Peg extends JButton {
	Color color;
	int position;
	
	/**
	 * Constructs the class.
	 * Sets <code>ImageIcon</code> and <code>color</code> to <code>EMO_COLOR</code> and position to given.
	 * Then defines the size to 100x25.
	 * 
	 * @param position position of the peg
	 */
	public Peg(int position) {
		super(new ImageIcon("images/" + EMO_COLOR.toString() + ".png"));
		this.color = EMO_COLOR;
		this.position = position;
		this.setPreferredSize(new Dimension(120, 50));
	}

	/**
	 * Returns the peg's color.
	 *
	 * @return the peg's color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Sets the peg's color.
	 * Changes the peg's <code>color</code> and <code>ImageIcon</code>.
	 *
	 * @param color the color to set the peg to
	 */
	public void setColor(Color color) {
		this.color = color;
		ImageIcon icon = new ImageIcon("images/" + this.color.toString() + ".png");
		this.setIcon(icon);
		this.setDisabledIcon(icon);
	}
	
	/**
	 * Returns the peg's position.
	 *
	 * @return int position of the peg
	 */
	public int getPosition() {
		return this.position;
	}
}
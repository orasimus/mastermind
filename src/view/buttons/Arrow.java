package view.buttons;

import javax.swing.JButton;
import javax.swing.ImageIcon;

/**
 * Game view's arrow button.
 */
public class Arrow extends JButton {
	/**
	 * Constructs the class.
	 * Set's <code>ImageIcon</code> as arrow.png.
	 */
	public Arrow() {
		super(new ImageIcon("images/arrow.png"));
	}
}
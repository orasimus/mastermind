package view.buttons;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Dimension;

import model.MasterGame.Color;

/**
 * Game view's color button.
 */
public class ColorButton extends JButton {
	Color color;

	/**
	 * Constructs the class.
	 * Sets <code>ImageIcon</code> and <code>color</code> to the given color.
	 */
	public ColorButton(Color color) {
		super(new ImageIcon("images/" + color.toString() + ".png"));
		this.color = color;
	}
	
	/**
	 * Returns the button's color.
	 *
	 * @return the button's color
	 */
	public Color getColor() {
		return color;
	}
}
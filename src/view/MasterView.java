package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

import static model.MasterGame.MAX_COLORS;
import static model.MasterGame.MAX_GUESSES;
import static model.MasterGame.MAX_PEGS_PER_ROUND;
import static model.MasterGame.EMO_COLOR;
import model.MasterGame.Color;

import view.buttons.*;

/**
 * MVC view.
 * Extends JFrame, so it is the main window of the GUI.
 */
public class MasterView extends JFrame {
	private JButton gameButton, scoreButton, exitButton;

	private ColorButton[] colorbuttons = new ColorButton[MAX_COLORS];
	private Peg[][] pegs = new Peg[MAX_GUESSES + 1][MAX_PEGS_PER_ROUND];
	private Arrow[] arrows = new Arrow[MAX_GUESSES];
	private JLabel[] hints = new JLabel[MAX_GUESSES];
	private	JLabel messageLabel;
	private JButton gameEndButton, scoreEndButton;
		
	private Color chosenColor;
	private Color[] allColors = Color.values();

	/**
	 * Constructs the class.
	 * Sets basic JFrame configuration, title as Mastermind and size resizable & 640x600.
	 * Then uses <code>createMenu()</code> to set menu as contentPane and sets itself visible.
	 */
	public MasterView() {
		JFrame.setDefaultLookAndFeelDecorated(true);
		this.setTitle("Mastermind");

		// This to controller event???
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.startMenu();

		this.setSize(640, 740);
		this.setResizable(false);
		this.setVisible(true);
	}

	/**
	 * Sets MasterView's contentPane to menu view.
	 */
	public void startMenu() {
		this.setVisible(false);
		this.setContentPane(this.createMenu());
		this.setVisible(true);
	}

	/**
	 * Sets MasterView's contentPane to game view.
	 */
	public void startGame() {
		this.setChosenColor(EMO_COLOR);
		this.setVisible(false);
		this.setContentPane(this.createGame());
		this.setVisible(true);
	}

	/**
	 * Sets MasterView's contentPane to score view.
	 */
	public void startScore(int[] score) {
		this.setVisible(false);
		this.setContentPane(this.createScore(score));
		this.setVisible(true);
	}

	/**
	 * Moves the game view to the next round.
	 * Renders the last round's pegs unusable and swaps the arrow with the hint.
	 * If the game ends, sets the <code>messageLabel</color> to a win/lose message.
	 * Otherwise if the game continues, sets the next round's pegs and arrow visible.
	 *
	 * @param round the next round
	 * @param hint last round's correct pegs (black, white)
	 */
	public void nextRound(int round, int[] hint) {
		this.disableRound(round - 1, hint);
		for (int i = 0; i < MAX_PEGS_PER_ROUND; i++) {
			pegs[round][i].setVisible(true);
			pegs[round][i].setEnabled(true);
			arrows[round].setVisible(true);
			arrows[round].setEnabled(true);
		}
	}
	
	/**
	 * Set's the game view to end state.
	 *
	 * @param hint the final round's hint
	 */
	public void endGame(int round, Color[] code, int[] hint) {
		this.disableRound(round, hint);
		if (hint[0] == MAX_PEGS_PER_ROUND) {
			messageLabel.setText("Voitit pelin!");
			messageLabel.setVisible(true);			
		} else {
			for (int i = 0; i < MAX_PEGS_PER_ROUND; i++) {
				pegs[MAX_GUESSES][i].setColor(code[i]);
				pegs[MAX_GUESSES][i].setVisible(true);
			}
			messageLabel.setText("H�visit pelin. Viimeisen� oikea koodi.");
			messageLabel.setVisible(true);
		}
	}
	
	/**
	 * Disables the last round's pegs and arrow, shows the hint.
	 *
	 * param round last round
	 * param hint hint to show
	 */
	private void disableRound(int round, int[] hint) {
		for (int i = 0; i < MAX_PEGS_PER_ROUND; i++)
			pegs[round][i].setEnabled(false);
		arrows[round].setVisible(false);
		arrows[round].setEnabled(false);
		hints[round].setIcon(new ImageIcon("images/" + hint[0] + hint[1] + ".png"));
		hints[round].setVisible(true);
	}
	
	/**
	 * Returns the current <code>chosenColor</code>
	 *
	 * @return Color the <code>chosenColor</code>
	 */
	public Color getChosenColor() {
		return this.chosenColor;
	}
	
	/**
	 * Sets the <code>chosenColor</code>
	 *
	 * @param color the color to set <code>chosenColor</code> to
	 */
	public void setChosenColor(Color color) {
		this.chosenColor = color;
	}
	
	/**
	 * Creates the <code>menuPanel</code> to be set as contentPane
	 */
	private JPanel createMenu() {
		JPanel menuPanel = new JPanel();
		menuPanel.setLayout(new BoxLayout(menuPanel, BoxLayout.PAGE_AXIS));

		menuPanel.add(Box.createRigidArea(new Dimension(0, 50)));

		JLabel title = new JLabel(new ImageIcon("images/mastermind.png"));
		title.setAlignmentX(Component.CENTER_ALIGNMENT);
		menuPanel.add(title);
		
		menuPanel.add(Box.createRigidArea(new Dimension(0, 40)));

		gameButton = new JButton("Uusi peli");
		gameButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		menuPanel.add(gameButton);

		scoreButton = new JButton("Pelitulokset");
		scoreButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		menuPanel.add(scoreButton);

		exitButton = new JButton("Lopeta");
		exitButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		menuPanel.add(exitButton);

		menuPanel.setOpaque(true);
		return menuPanel;
	}

	/**
	 * Creates the <code>gamePanel</code> to be set as contentPane
	 */
	private JPanel createGame() {
		JPanel gamePanel = new JPanel();
		gamePanel.setLayout(new BoxLayout(gamePanel, BoxLayout.PAGE_AXIS));

		gamePanel.add(Box.createRigidArea(new Dimension(0, 10)));

		JPanel colorPanel = new JPanel();
		colorPanel.setLayout(new BoxLayout(colorPanel, BoxLayout.LINE_AXIS));	
		gamePanel.add(colorPanel);

		for (int i = 0; i < MAX_COLORS; i++) {
			colorbuttons[i] = new ColorButton(allColors[i]);
			colorPanel.add(colorbuttons[i]);
			colorPanel.add(Box.createRigidArea(new Dimension(10, 0)));
		}

		gamePanel.add(Box.createVerticalGlue());

		JPanel guessPanel = new JPanel();
		guessPanel.setLayout(new BoxLayout(guessPanel, BoxLayout.LINE_AXIS));

		guessPanel.add(Box.createHorizontalGlue());

		JPanel guessGrid = new JPanel();
		guessGrid.setLayout(new GridBagLayout());
		guessPanel.add(guessGrid);
		
		GridBagConstraints c = new GridBagConstraints();

		JPanel[] hintPanels = new JPanel[MAX_GUESSES];

		for (int i = 0; i < MAX_GUESSES; i++) {
			c.ipadx = 5;
			c.ipady = 5;
			c.gridy = i;
			for (int j = 0; j < MAX_PEGS_PER_ROUND; j++) {
				pegs[i][j] = new Peg(j);
				pegs[i][j].setVisible(false);
				pegs[i][j].setEnabled(false);
				c.gridx = j;
				guessGrid.add(pegs[i][j], c);
			}
			
			c.gridx++;
			hintPanels[i] = new JPanel();
			hintPanels[i].setPreferredSize(new Dimension(50, 50));

			guessGrid.add(hintPanels[i], c);

			arrows[i] = new Arrow();
			arrows[i].setVisible(false);
			arrows[i].setEnabled(false);
			hintPanels[i].add(arrows[i]);

			hints[i] = new JLabel();
			hints[i].setVisible(false);
			hintPanels[i].add(hints[i]);
		}
		
		c.gridy++;
		c.gridx = 0;
		for (int i = 0; i < MAX_PEGS_PER_ROUND; i++) {
			pegs[MAX_GUESSES][i] = new Peg(i);
			pegs[MAX_GUESSES][i].setVisible(false);
			pegs[MAX_GUESSES][i].setColor(Color.RED);
			pegs[MAX_GUESSES][i].setEnabled(false);
			guessGrid.add(pegs[MAX_GUESSES][i], c);
			c.gridx++;
		}

		for (int i = 0; i < MAX_PEGS_PER_ROUND; i++) {
			pegs[0][i].setVisible(true);
			pegs[0][i].setEnabled(true);
		}

		arrows[0].setVisible(true);
		arrows[0].setEnabled(true);

		guessPanel.add(Box.createHorizontalGlue());
		gamePanel.add(guessPanel);
		
		gamePanel.add(Box.createVerticalGlue());

		messageLabel = new JLabel("");
		messageLabel.setVisible(false);
		messageLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		gamePanel.add(messageLabel, BorderLayout.CENTER);

		JPanel endPanel = new JPanel();
		endPanel.setLayout(new BoxLayout(endPanel, BoxLayout.LINE_AXIS));
		gamePanel.add(endPanel);

		endPanel.add(Box.createHorizontalGlue());

		gameEndButton = new JButton("lopetus");
		endPanel.add(gameEndButton);

		gamePanel.setOpaque(true);
		return gamePanel;
	}

	/**
	 * Creates the <code>scorePanel</code> to be set as contentPane
	 *
	 * @return JPanel created scorePanel
	 */
	private JPanel createScore(int[] score) {
		JPanel scorePanel = new JPanel();
		scorePanel.setLayout(new BoxLayout(scorePanel, BoxLayout.PAGE_AXIS));

		scorePanel.add(Box.createRigidArea(new Dimension(0, 50)));

		JLabel title = new JLabel(new ImageIcon("images/pelitulokset.png"));
		title.setAlignmentX(Component.CENTER_ALIGNMENT);
		scorePanel.add(title);

		scorePanel.add(Box.createRigidArea(new Dimension(0, 40)));

		JLabel totalLabel = new JLabel("Pelej� on pelattu yhteens�: " + score[0]);
		totalLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		scorePanel.add(totalLabel);
		
		scorePanel.add(Box.createRigidArea(new Dimension(0, 20)));
		
		JLabel wonLabel = new JLabel("");
		if (score[1] == 0)
			wonLabel.setText("...joista yht��n kertaa pelaaja ei ole arvannut oikeaa koodia...");
		else if (score[1] == 1)
			wonLabel.setText("...joista yhden kerran pelaaja on arvannut oikean koodin...");
		else
			wonLabel.setText("...joista " + score[1] + " kertaa pelaaja on arvannut oikean koodin...");
		wonLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		scorePanel.add(wonLabel);
		
		scorePanel.add(Box.createRigidArea(new Dimension(0, 20)));
		
		JLabel lostLabel = new JLabel("");
		if (score[2] == 0)
			lostLabel.setText("...ja joista kertaakaan pelaaja ei ole ep�onnistunut koodin arvaamisessa.");
		else if (score[2] == 1)
			lostLabel.setText("...ja joista yhden kerran pelaaja on ep�onnistunut koodin arvaamisessa.");
		else
			lostLabel.setText("...ja joista pelaaja on ep�onnistunut koodin arvaamisessa " + score[2] + " kertaa.");
		lostLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		scorePanel.add(lostLabel);
		
		scorePanel.add(Box.createRigidArea(new Dimension(0, 40)));
		
		scoreEndButton = new JButton("takaisin");
		scoreEndButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		scorePanel.add(scoreEndButton);
		
		scorePanel.setOpaque(true);
		return scorePanel;
	}

	/**
	 * Adds given <code>ActionListener</code> to menu view's buttons
	 *
	 * @param l <code>ActionListener</code>
	 */
	public void addMenuButtonListeners(ActionListener l) {
		gameButton.addActionListener(l);
		scoreButton.addActionListener(l);
		exitButton.addActionListener(l);
	}
	
	/**
	 * Adds given <code>ActionListener</code> to game view's ColorButtons
	 *
	 * @param l <code>ActionListener</code>
	 */
	public void addColorButtonListeners(ActionListener l) {
		for (int i = 0; i < MAX_COLORS; i++) {
				colorbuttons[i].addActionListener(l);
		}
	}
	
	/**
	 * Adds given <code>ActionListener</code> to game view's Pegs
	 *
	 * @param l <code>ActionListener</code>
	 */
	public void addPegListeners(ActionListener l) {
		for (int i = 0; i < MAX_GUESSES; i++) {
			for (int j = 0; j < MAX_PEGS_PER_ROUND; j++) {
				pegs[i][j].addActionListener(l);
			}
		}
	}
	
	/**
	 * Adds given <code>ActionListener</code> to game view's Arrows
	 *
	 * @param l <code>ActionListener</code>
	 */
	public void addArrowListeners(ActionListener l) {
		for (int i = 0; i < MAX_GUESSES; i++) {
			arrows[i].addActionListener(l);
		}
	}
	
	/**
	 * Adds given <code>ActionListener</code> to game view's <code>gameEndButton</code>
	 *
	 * @param l <code>ActionListener</code>
	 */
	public void addGameEndButtonListeners(ActionListener l) {
		gameEndButton.addActionListener(l);
	}
	
	/**
	 * Adds given <code>ActionListener</code> to game view's <code>scoreEndButton</code>
	 *
	 * @param l <code>ActionListener</code>
	 */
	public void addScoreEndButtonListeners(ActionListener l) {
		scoreEndButton.addActionListener(l);
	}
}